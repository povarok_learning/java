package com.hw2.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static List<Integer> getNumbers() {
        Scanner scanner = new Scanner(System.in);
        String sequence = scanner.nextLine();
        String[] textNumbers = sequence.trim().replaceAll(" +", " ").split(" ");
        List<Integer> numbers=new ArrayList<>();

        for(var e: textNumbers){
            numbers.add(Integer.parseInt(e));
        }

        return numbers;
    }
    public static boolean canEqualArray(List<Integer> array, int equaler) {
        var min = Collections.min(array);
        var max = Collections.max(array);
        var mid = (max+min)/2;
        for(int y : array) if(equaler != 0 && y % equaler != 0 || y < mid - equaler || y > mid + equaler) return false;
        return true;
    }
    public static int calculate(List<Integer> numbers) {
        var candidates = numbers
                .stream()
                .filter(e->canEqualArray(numbers, e))
                .collect(Collectors.toList());;

        return candidates.size() > 0 ? candidates.get(candidates.indexOf(Collections.min(candidates))) : -1;
    }

    public static void main(String[] args) {
        System.out.println("Enter numbers!");
        var numbers = getNumbers();
        System.out.println(calculate(numbers));
    }
}