package com.hw2.task1;

import java.util.*;



public class Main {
    public static List<String[]> getScores() {
        Scanner scanner = new Scanner(System.in);
        String sequence = scanner.nextLine();
        String[] textScores = sequence.replace("\"","").split(",");
        List<String[]> scores = new ArrayList<>();

        for (String score: textScores) {
            String[] p = score.split(" ");
            scores.add(p);
        }

        return scores;
    }

    public static String getWinner(List<String[]> scores) {
        var maxPoints = 0;
        Map<String,Integer> maxPointsTable=new HashMap<>();
        for (var e: scores) {
            var name = e[0];
            var score = Integer.parseInt(e[1]);
            int playerScore = maxPointsTable.containsKey(name) ? maxPointsTable.get(name) + score : score;
            maxPoints = java.lang.Math.max(maxPoints, playerScore);
            maxPointsTable.put(name, playerScore);
        }
        System.out.println("Max points: " + maxPoints);

        String winner = null;
        Map<String,Integer> scoreTable=new HashMap<>();
        for (var e: scores) {
            var name = e[0];
            var score = Integer.parseInt(e[1]);
            int playerScore = scoreTable.containsKey(name) ? scoreTable.get(name) + score : score;
            if (playerScore >= maxPoints) {
                winner = name;
                break;
            } else {
                scoreTable.put(name, playerScore);
            }
        }
        return winner;
    }

    public static void main(String[] args) {
        // input string example: "Ivan 5","Petr 3","Alex 10","Petr 8","Ivan 6","Alex 5","Ivan 1","Petr 5","Alex 1"
        System.out.println("Enter scores:");
        List<String[]> scores = getScores();
        System.out.println("Winner is " + getWinner(scores));
    }
}