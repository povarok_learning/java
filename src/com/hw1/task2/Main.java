package com.hw1.task2;

public class Main {

    public static Storage storage =  new Storage();
    public static Buyer buyer =  new Buyer(storage);
    public static Provider provider = new Provider(storage);

    public static void log() {
        System.out.println("Storage fill count: " + storage.getGoodsCount().toString());
    }

    public static void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Thread(buyer).start();
        new Thread(provider).start();
        while (true) {
            Main.log();
            Main.sleep();
        }
    }
}
