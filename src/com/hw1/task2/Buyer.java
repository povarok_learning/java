package com.hw1.task2;

public class Buyer implements Runnable {
    public Storage storage;

    public Buyer(Storage storage) {
        this.storage = storage;
    }

    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this.storage) {
                Integer goodsCount = this.storage.getGoodsCount();
                if (goodsCount > 0) {
                    this.storage.setGoodsCount(goodsCount - 1);
                }
            }
            this.sleep();

        }
    }
}
