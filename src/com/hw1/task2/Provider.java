package com.hw1.task2;

public class Provider implements Runnable {
    public Storage storage;

    public Provider(Storage storage) {
        this.storage = storage;
    }

    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this.storage) {
                Integer goodsCount = this.storage.getGoodsCount();
                if (goodsCount <= 5) { this.storage.setGoodsCount(goodsCount + 5); }
            }
            this.sleep();
        }
    }
}
