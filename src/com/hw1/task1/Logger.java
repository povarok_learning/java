package com.hw1.task1;

public class Logger implements Runnable {
    private Integer logInterval;
    private Counter counter;


    public Logger(Integer logInterval, Counter counter) {
        this.logInterval = logInterval;
        this.counter = counter;
    }

    public void log() {
        System.out.println("Logger (" + this.logInterval.toString() + "): " + this.counter.getCount());
    }

    @Override
    public void run() {
        while (true) {
            log();
            try {
                Thread.sleep(this.logInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
