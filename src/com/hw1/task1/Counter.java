package com.hw1.task1;

public class Counter {
    public Integer count;

    public Integer getCount() {
        return count;
    }

    public Counter(int initCount) {
        this.count = initCount;
    }
    public int increment() {
        count ++;
        return count;
    }
}
