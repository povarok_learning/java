package com.hw1.task1;

public class Timer implements Runnable {
    public Counter counter;
    private Thread[] loggerList;

    public Timer(Counter counter) {
        this.counter = counter;
        this.loggerList = new Thread[]{
            new Thread(new Logger(1000, this.counter)),
            new Thread(new Logger(5000, this.counter))
        };
        for (Thread logger:this.loggerList) { logger.start(); }
    }

    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("start thread");
        while (true) {
            this.counter.increment();
            this.sleep();
        }
    }
}
